/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function ()
{
	"use strict";

	sap.ui.require([
		"ZITI/LogiMat2020CheckIn/test/integration/AllJourneys"
	], function ()
	{
		QUnit.start();
	});
});