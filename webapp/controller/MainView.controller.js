sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/resource/ResourceModel",
	"sap/ui/model/odata/v2/ODataModel",
	"sap/m/MessageBox",
	"sap/ndc/BarcodeScanner"
], function (Controller, JSONModel, ResourceModel, ODataModel, MessageBox, BarcodeScanner) {
	"use strict";

	var driverData = {};
	var currentPage = 1;
	var lang;
	var wrong;
	return Controller.extend("ZITI.LogiMat2020CheckIn.controller.WelcomeView", {
		onInit: function () {

			//Initialize Global Model 
			var oGlobalModel2 = sap.ui.getCore().getModel("globalVariable");
			this.getView().setModel(oGlobalModel2, "globalVariable");
			this.getView().byId("forwardButton").setText(this.localize("start"));
			var that = this;
			var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
			if (sCurrentLocale.includes("de")) { // if de-DE or de-AT
				sap.ui.getCore().getModel("globalVariable").setProperty("/actualLanguageImage", "images/icons8-germany-96.png");
			} else if (sCurrentLocale.includes("en")) {
				sap.ui.getCore().getModel("globalVariable").setProperty("/actualLanguageImage", "images/icons8-great-britain-96.png");
			} else if (sCurrentLocale.includes("pl")) {
				sap.ui.getCore().getModel("globalVariable").setProperty("/actualLanguageImage", "images/icons8-poland-96.png");
			} else if (sCurrentLocale.includes("ru")) {
				sap.ui.getCore().getModel("globalVariable").setProperty("/actualLanguageImage", "images/icons8-russian-federation-96.png");
			} else if (sCurrentLocale.includes("cz")) {
				sap.ui.getCore().getModel("globalVariable").setProperty("/actualLanguageImage", "images/icons8-czech-republic-96.png");
			}

			sap.ui.getCore().attachLocalizationChanged(function (oEvent) {
				var oChanges = oEvent.getParameter("changes");
				if (oChanges && oChanges.language) {
					that._bundle = sap.ui.getCore().getLibraryResourceBundle("sap.m", oChanges.language);
					that.getView().rerender();

				}
			});

			var parentURL;
			try {
				parentURL = window.parent.location.href;
			} catch (e) {
				//nothing
			}

			if (parentURL) {
				sap.ui.getCore().getModel("globalVariable").setProperty("/showHeader", true);
			}

		},

		onChangeZoomLevel: function (oEvent) {

			var zoomLevel = oEvent.getParameters().selectedItem.getKey();

			try {
				document.body.style.zoom = zoomLevel;
				this.getView().getModel("globalVariable").setProperty("/actualZoom", zoomLevel);
			} catch (e) {
				//
			}
		},

		openSelectLanguagePopover: function (oEvent) {

			// create popover
			if (!this._oSelectLanguagePopover) {
				this._oSelectLanguagePopover = sap.ui.xmlfragment("ZITI.LogiMat2020CheckIn..view.SelectLanguage", this);
				this.getView().addDependent(this._oSelectLanguagePopover);
			}

			this._oSelectLanguagePopover.openBy(oEvent.getSource());
		},

		onSelectLanugagePopOverButtonPress: function (oEvent) {
			this.setLanguage(oEvent);
			this._oSelectLanguagePopover.close();
		},

		onLanguageBoxPress: function (oEvent) {
			this.setLanguage(oEvent);
			this.onNextButtonPress();
		},

		setLanguage: function (oEvent, page) {
			var that = this;
			var id = oEvent.getParameters().id;
			var bindingContextPath = sap.ui.getCore().byId(id).getBindingContextPath();
			var bindingModel = that.getView().getModel("globalVariable").getProperty(bindingContextPath);
			that.getView().getModel("globalVariable").setProperty("/actualLanguageImage", bindingModel.roundFlag);
			sap.ui.getCore().getConfiguration().setLanguage(bindingModel.key);
			if (currentPage === 1) {
				this.getView().byId("forwardButton").setText(this.localize("start"));
			}
			if (currentPage === 5) {
				this.getView().byId("forwardButton").setText(this.localize("end"));
			}
			if (bindingModel.key === "cz") {
				this.getView().byId("mobileInput").setValue("+420");
				wrong = " není kód vaší sms";
			}
			if (bindingModel.key === "pl") {
				this.getView().byId("mobileInput").setValue("+48");
				wrong = " nie jest kodem w Twoim sms";
			}
			if (bindingModel.key === "en") {
				this.getView().byId("mobileInput").setValue("+44");
				wrong = " is not the code in your sms";
			}
			if (bindingModel.key === "de") {
				this.getView().byId("mobileInput").setValue("+49");
				wrong = " ist nicht der Code in Ihrer SMS";
			}
			if (bindingModel.key === "ru") {
				this.getView().byId("mobileInput").setValue("+7");
				wrong = " это не код в твоем смс";
			}
			if (bindingModel.key === "sp") {
				this.getView().byId("mobileInput").setValue("+34");
				wrong = " no es el código en tu sms";
			}
			if (bindingModel.key === "hun") {
				this.getView().byId("mobileInput").setValue("+36");
				wrong = " nem az SMS-ben szereplő kód";
			}
			if (bindingModel.key === "rou") {
				this.getView().byId("mobileInput").setValue("+40");
				wrong = " nu este codul din SMS-urile tale";
			}
			if (bindingModel.key === "bgr") {
				this.getView().byId("mobileInput").setValue("+359");
				wrong = " не е кодът във вашия sms";
			}
			lang = bindingModel.key;

		},

		goToPage: function (page) {
			var targetPage = "page" + page;
			var that = this;
			this.getView().byId("navCon").to(this.getView().byId(targetPage));
			currentPage = page;
			if (page === 1) {
				this.getView().byId("forwardButton").setText(this.localize("start"));
				this.getView().byId("forwardButton").setEnabled(true);
				this.getView().byId("backButton").setEnabled(false);
				this.getView().byId("cancelButton").setEnabled(false);
			} else if (page === 2) {
				this.getView().byId("forwardButton").setText(this.localize("continue"));
				this.getView().byId("backButton").setEnabled(true);
				this.getView().byId("cancelButton").setEnabled(true);

				if (this.getView().byId("orderNumberInput").getValue() === "") {
					this.getView().byId("orderNumberInput").setValueState("Information");
				}
			} else if (page === 3) {
				if (this.getView().byId("firstNameInput").getValue() === "") {
					this.getView().byId("firstNameInput").setValueState("Information");
				}
				if (this.getView().byId("lastNameInput").getValue() === "") {
					this.getView().byId("lastNameInput").setValueState("Information");
				}
				if (this.getView().byId("licensePlateInput").getValue() === "") {
					this.getView().byId("licensePlateInput").setValueState("Information");
				}
			} else if (page === 6) {
				this.getView().byId("forwardButton").setEnabled(true);
				this.getView().byId("forwardButton").setText(this.localize("end"));

				var number = this.getView().byId("orderNumberInput").getValue();
				var url = "http://chart.apis.google.com/chart?cht=qr&chs=250x250&chl=OrderNumber:" + number + ";;";
				var mywindow = window.open('', '', 'height=900,width=900');

				mywindow.document.write(
					'<h1 style="text-align:center;">Aryzta Check-in</h1><br><img style="margin-left:30%; margin-right:35%;" src=' + url +
					' width="300" height="300" id="qrCode">');
				mywindow.print();

				setTimeout(function demo() {
					that.GoBacktoStart(this);
				}, 7000);

			}
		},

		onCancelButtonPress: function (oEvent) {
			driverData = {};
			this.getView().byId("orderNumberInput").setValue("");
			this.getView().byId("firstNameInput").setValue("");
			this.getView().byId("lastNameInput").setValue("");
			this.getView().byId("licensePlateInput").setValue("");
			this.getView().byId("trailerPlateInput").setValue("");
			this.getView().byId("mobileInput").setValue("");
			this.getView().byId("codeInput").setValue("");
			this.goToPage(1);
		},

		onBackButtonPress: function (oEvent) {
			if (currentPage > 1) {
				this.goToPage(currentPage - 1);
			}
		},

		onNextButtonPress: function (oEvent) {
			if (currentPage < 7) {
				if (currentPage === 2) {
					this.onStep2Submit(undefined);
				} else if (currentPage === 3) {
					this.onStep3Submit(undefined);
				} else if (currentPage === 4) {
					this.onStep4Submit(undefined);
				} else if (currentPage === 5) {
					this.onStep5Submit(undefined);
				} else if (currentPage === 6) {
					driverData = {};
					this.getView().byId("orderNumberInput").setValue("");
					this.getView().byId("firstNameInput").setValue("");
					this.getView().byId("lastNameInput").setValue("");
					this.getView().byId("licensePlateInput").setValue("");
					this.getView().byId("trailerPlateInput").setValue("");
					this.getView().byId("mobileInput").setValue("");
					this.getView().byId("codeInput").setValue("");
					this.goToPage(1);
				} else {
					this.goToPage(currentPage + 1);
				}
			}

		},

		GoBacktoStart: function () {
			driverData = {};
			this.getView().byId("orderNumberInput").setValue("");
			this.getView().byId("firstNameInput").setValue("");
			this.getView().byId("lastNameInput").setValue("");
			this.getView().byId("licensePlateInput").setValue("");
			this.getView().byId("trailerPlateInput").setValue("");
			this.getView().byId("mobileInput").setValue("");
			this.getView().byId("codeInput").setValue("");
			this.goToPage(1);
		},

		isNullOrWhitespace: function (input) {

			if (typeof input === "undefined" || input === null) return true;

			return input.replace(/\s/g, "").length < 1;
		},

		onStep2Submit: function (oEvent) {
			var length;
			var str;
			var er;

			if (!this.isNullOrWhitespace(this.getView().byId("orderNumberInput").getValue())) {
				length = this.getView().byId("orderNumberInput").getValue().length;
				if (length > 10) {
					str = length = this.getView().byId("orderNumberInput").getValue();
					er = str.slice(0, 10);
					this.getView().byId("orderNumberInput").setValue(er);
				}
				driverData.Auftragsnummer = this.getView().byId("orderNumberInput").getValue();
				var oModel = this.getView().getModel();
				oModel.create('/AryztaSet', driverData, null, function () {},
					function () {});
				this.goToPage(currentPage + 1);
			} else {
				if (this.isNullOrWhitespace(this.getView().byId("orderNumberInput").getValue())) {
					this.getView().byId("orderNumberInput").setValueState("Error"); // if the field is empty after change, it will go red
				} else {
					this.getView().byId("orderNumberInput").setValueState("None"); // if the field is not empty after change, the value state (if any) is removed
				}
			}
			// this.messageToast(this.getView().getModel("i18n").getResourceBundle().getText(), undefined);
		},

		onStep3Submit: function (oEvent) {
			var length;
			var str;
			var er;

			if (!this.isNullOrWhitespace(this.getView().byId("licensePlateInput").getValue()) && !this.isNullOrWhitespace(this.getView().byId(
					"firstNameInput").getValue()) && !this.isNullOrWhitespace(this.getView()
					.byId("lastNameInput").getValue())) {

				length = this.getView().byId("mobileInput").getValue().length;
				if (length > 15) {
					str = length = this.getView().byId("mobileInput").getValue();
					er = str.slice(0, 15);
					this.getView().byId("mobileInput").setValue(er);
				}
				length = this.getView().byId("firstNameInput").getValue().length;
				if (length > 35) {
					str = length = this.getView().byId("firstNameInput").getValue();
					er = str.slice(0, 35);
					this.getView().byId("firstNameInput").setValue(er);
				}
				length = this.getView().byId("lastNameInput").getValue().length;
				if (length > 35) {
					str = length = this.getView().byId("lastNameInput").getValue();
					er = str.slice(0, 35);
					this.getView().byId("lastNameInput").setValue(er);
				}
				length = this.getView().byId("licensePlateInput").getValue().length;
				if (length > 10) {
					str = length = this.getView().byId("licensePlateInput").getValue();
					er = str.slice(0, 10);
					this.getView().byId("licensePlateInput").setValue(er);
				}
				length = this.getView().byId("trailerPlateInput").getValue().length;
				if (length > 10) {
					str = length = this.getView().byId("trailerPlateInput").getValue();
					er = str.slice(0, 10);
					this.getView().byId("trailerPlateInput").setValue(er);
				}

				driverData.Telefon = this.getView().byId("mobileInput").getValue();
				driverData.KfzKennzeichen = this.getView().byId("licensePlateInput").getValue();
				driverData.LastzugKennzeichen = this.getView().byId("trailerPlateInput").getValue();
				driverData.Vorname = this.getView().byId("firstNameInput").getValue();
				driverData.Nachname = this.getView().byId("lastNameInput").getValue();
				var oModel = this.getView().getModel();
				oModel.create('/AryztaSet', driverData, null, function () {},
					function () {});

				this.goToPage(currentPage + 1);
			} else {

				if (this.isNullOrWhitespace(this.getView().byId("firstNameInput").getValue())) {
					this.getView().byId("firstNameInput").setValueState("Error"); // if the field is empty after change, it will go red
				} else {
					this.getView().byId("firstNameInput").setValueState("None"); // if the field is not empty after change, the value state (if any) is removed
				}
				if (this.isNullOrWhitespace(this.getView().byId("lastNameInput").getValue())) {
					this.getView().byId("lastNameInput").setValueState("Error"); // if the field is empty after change, it will go red
				} else {
					this.getView().byId("lastNameInput").setValueState("None"); // if the field is not empty after change, the value state (if any) is removed
				}
				if (this.isNullOrWhitespace(this.getView().byId("licensePlateInput").getValue())) {
					this.getView().byId("licensePlateInput").setValueState("Error"); // if the field is empty after change, it will go red
				} else {
					this.getView().byId("licensePlateInput").setValueState("None"); // if the field is not empty after change, the value state (if any) is removed
				}
			}
		},

		onStep4Submit: function (oEvent) {
			driverData.Telefon = this.getView().byId("mobileInput").getValue();
			driverData.Language = lang;
			var oModel = this.getView().getModel();
			oModel.create('/AryztaSet', driverData, null, function () {},
				function () {});
			this.goToPage(currentPage + 1);
		},

		onStep5Submit: function (oEvent) {
			var oModel = this.getView().getModel();
			// oModel.read('/AryztaSet', driverData, null, function (oData) {},
			// 	function () {});

			var code = this.getView().byId("codeInput").getValue();
			var sms = oModel.getProperty("/AryztaSet('" + driverData.Auftragsnummer + "')/SMS");

			if (code === sms) {
				this.goToPage(currentPage + 1);
			} else {
				this.messageToast(code + wrong, undefined);
				this.getView().byId("codeInput").setValueState("Error");
			}
		},

		onLiveChange: function (oEvent) {
			var newValue = oEvent.getParameter("newValue");
			var id = oEvent.getParameter("id");
			if (this.isNullOrWhitespace(newValue)) {
				this.getView().byId(id).setValueState("Error"); // if the field is empty after change, it will go red
			} else {
				this.getView().byId(id).setValueState("None"); // if the field is not empty after change, the value state (if any) is removed
			}
		},

		localize: function (key) {
			return this.getView().getModel("i18n").getResourceBundle().getText(key);
		},

		messageToast: function (key, type) {
			var msg = this.localize(key);
			sap.m.MessageToast.show(msg, {
				duration: 4000
			});
		}

		// qrScannerRequest: function () {
		// 	var that = this;
		// 	var containerId = this.getView().byId("qrContainer").sId;
		// 	this.getView().byId("scannButton").setVisible(false);
		// 	this.getView().byId("forwardButton").setEnabled(false);
		// 	if (containerId) {
		// 		qrCodeReader = new Html5Qrcode(containerId);
		// 		// This method will trigger user permissions
		// 		Html5Qrcode.getCameras().then(function (devices) {
		// 			if (devices && devices.length) {
		// 				currentCameraIdx = 0;
		// 				cameraId = devices[currentCameraIdx].id;
		// 				//	text.match("back");
		// 				devicesList = devices;
		// 				that.qrScannerStart(cameraId);
		// 			}
		// 		}).catch(function (error) {
		// 			console.error(error);
		// 		});
		// 	} else {
		// 		console.error("Unable to find QR container ID.");
		// 		this.getView().byId("forwardButton").setEnabled(true);
		// 	}
		// },

		// changeCamera: function () {
		// 	var nextCamIdx = currentCameraIdx + 1;
		// 	this.qrScannerStop(undefined, undefined);
		// 	if (nextCamIdx >= devicesList.length) {
		// 		nextCamIdx = 0;
		// 	}
		// 	currentCameraIdx = nextCamIdx;
		// 	this.qrScannerStart(devicesList[currentCameraIdx].id);
		// },

		// qrScannerStart: function (cameraId) {
		// 	var that = this;
		// 	if (devicesList.length > 1) {
		// 		this.getView().byId("changeCamButton").setVisible(true);
		// 	} else {
		// 		this.getView().byId("changeCamButton").setVisible(false);
		// 	}
		// 	//	this.getView().byId("stopScanButton").setVisible(true);
		// 	//	this.getView().byId("forwardButton").setEnabled(false);
		// 	//	this.getView().byId("backButton").setEnabled(false);

		// 	//	this.getView().byId("cancelButton").setEnabled(false);
		// 	qrCodeReader.start(
		// 		cameraId, {
		// 			fps: 10, // Optional frame per seconds for qr code scanning
		// 			qrbox: 250 // Optional if you want bounded box UI
		// 		},
		// 		function (result) {
		// 			driverData.Url = result;
		// 			that.qrScannerStop(undefined, result);

		// 		}).catch(function (error) {
		// 		// do nothing
		// 	});
		// },

		// qrScannerStop: function (event, result) {
		// 	var that = this;

		// 	qrCodeReader.stop().then(function (ignore) {
		// 		// QR Code scanning is stopped.
		// 		if (event) {
		// 			// do nothing
		// 			that.getView().byId("forwardButton").setEnabled(true);
		// 			that.getView().byId("backButton").setEnabled(true);
		// 			that.getView().byId("cancelButton").setEnabled(true);
		// 			//	that.getView().byId("stopScanButton").setVisible(false);
		// 			that.getView().byId("changeCamButton").setVisible(false);
		// 			that.getView().byId("scannButton").setVisible(true);
		// 			that.onBackButtonPress();
		// 		}
		// 		if (result) {
		// 			console.log(result);
		// 			that.onStep4Submit(undefined);
		// 		}
		// 	}).catch(function (error) {
		// 		console.error(error);
		// 		// Stop failed, handle it.
		// 	});

	});
});